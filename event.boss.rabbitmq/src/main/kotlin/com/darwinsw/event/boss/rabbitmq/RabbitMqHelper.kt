package com.darwinsw.event.boss.rabbitmq

object RabbitMqHelper {
    const val Durable = true
    const val NotExclusive = false
    const val DoNotAutoDelete = false
    const val Single = false
    const val NoAutoAck = false
    const val NoRoutingKey = ""

    const val MessageTypeHeaderKey = "message-type"
}