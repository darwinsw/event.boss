package com.darwinsw.event.boss.rabbitmq

import com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es.MessagePublisher
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.MessageTypeHeaderKey
import com.rabbitmq.client.BuiltinExchangeType
import com.rabbitmq.client.Connection
import com.rabbitmq.client.ConnectionFactory
import com.rabbitmq.client.MessageProperties
import java.net.URI

class RabbitMqMessagePublisher(config : RabbitMqConfiguration) : MessagePublisher {
    private val factory : ConnectionFactory = ConnectionFactory()
    private var connection : Connection? = null

    init {
        factory.setUri(URI(config.connectionString))
    }

    fun connect() {
        connection = factory.newConnection()
    }

    fun disconnect() {
        connection?.close()
    }

    private fun ensureConnection() : Connection {
        if(connection == null) {
            connection = factory.newConnection()
        }
        return connection!!
    }

    override fun publish(msgType: String, data: ByteArray) {
        val conn = ensureConnection()
        val channel = conn.createChannel()
        channel.exchangeDeclare(msgType, BuiltinExchangeType.FANOUT)
        val headers = mutableMapOf<String, Any>(MessageTypeHeaderKey to msgType)
        val properties = MessageProperties.PERSISTENT_BASIC.builder()
                .headers(headers).build()
        channel.basicPublish(msgType, "", properties, data)
        channel.close()
    }
}