package com.darwinsw.event.boss.rabbitmq

data class RabbitMqConfiguration(val connectionString: String)