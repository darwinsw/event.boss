package com.darwinsw.event.boss.rabbitmq

import com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es.DomainEventSerializer
import com.darwinsw.event.boss.core.es.DomainEvent
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.DoNotAutoDelete
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.Durable
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.MessageTypeHeaderKey
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.NoAutoAck
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.NoRoutingKey
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.NotExclusive
import com.darwinsw.event.boss.rabbitmq.RabbitMqHelper.Single
import com.rabbitmq.client.*
import org.reflections.Reflections
import org.reflections.util.ClasspathHelper
import org.reflections.util.ConfigurationBuilder
import java.net.URI

private const val UnnamedSubscriber = ""
private const val NoPrefix = ""

class RabbitMqEventSubscriber(config : RabbitMqConfiguration, private val serializer : DomainEventSerializer,
                              private val queuesPrefix: String = "") {

    private val factory : ConnectionFactory = ConnectionFactory()
    private var connection : Connection? = null

    init {
        factory.setUri(URI(config.connectionString))
    }

    fun connect() {
        connection = factory.newConnection()
    }

    fun disconnect() {
        connection?.close()
    }

    private fun ensureConnection() : Connection {
        if(connection == null) {
            connection = factory.newConnection()
        }
        return connection!!
    }

    fun <TEvent : DomainEvent<*>> subscribe(clz: Class<TEvent>, handler: (TEvent) -> Unit) {
        subscribe(UnnamedSubscriber, clz, handler)
    }

    fun <TEvent : DomainEvent<*>> subscribe(subName: String, clz: Class<TEvent>, handler: (TEvent) -> Unit) {
        val conn = ensureConnection()
        val channel = conn.createChannel()
        val eventFullName = eventType(clz)
        val rootPrefix = if (queuesPrefix != "") "$queuesPrefix." else NoPrefix
        val subPrefix = if(subName != "") "$subName." else NoPrefix
        val prefix = "$rootPrefix$subPrefix"

        val queueName = prefix + eventFullName
        channel.queueDeclare(queueName, Durable, NotExclusive, DoNotAutoDelete, emptyMap())
        createExchangeAndBindToQueue(channel, clz, queueName)
        for(subType in subTypesOf(clz)) {
            createExchangeAndBindToQueue(channel, subType, queueName)
        }

        val callback : (String, Delivery) -> Unit = { consumerTag, delivery ->
            val msgType = typeFromDelivery(delivery) ?: queueName
            val data = rehydrateDomainEvent(String(delivery.body), msgType)
            handler(data as TEvent)
            channel.basicAck(delivery.envelope.deliveryTag, Single)
        }
        channel.basicConsume(queueName, NoAutoAck, callback) { tag -> }
    }

    private fun typeFromDelivery(msg: Delivery) = msg.properties.headers.getValue(MessageTypeHeaderKey)?.toString()

    private fun createExchangeAndBindToQueue(channel:Channel, type: Class<*>, queueName: String) {
        val exchangeName = eventType(type)
        channel.exchangeDeclare(exchangeName, BuiltinExchangeType.FANOUT)
        channel.queueBind(queueName, exchangeName, NoRoutingKey)
    }

    private fun eventType(clz: Class<*>) = clz.canonicalName!!

    private fun rehydrateDomainEvent(data: String, type: String?) =
            serializer.deserialize(data, Class.forName(type)) as DomainEvent<*>

    companion object {
        private val reflections = Reflections(ConfigurationBuilder().addUrls(ClasspathHelper.forPackage("com.darwinsw.event.boss")))

        fun subTypesOf(clz: Class<*>): Set<Class<*>> = reflections.getSubTypesOf(clz)
    }
}