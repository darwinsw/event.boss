package com.darwinsw.event.boss.mongodb

import com.darwinsw.event.boss.core.persistence.OpaqueRepository
import com.mongodb.ConnectionString
import com.mongodb.client.MongoCollection
import com.mongodb.client.model.ReplaceOptions
import com.mongodb.client.model.UpdateOptions
import org.bson.BsonDocument
import org.litote.kmongo.KMongo


open class MongoDbOpaqueRepository<T : Any>(val config : MongoDbConfiguration, val collectionName: String, val clz: Class<T>) : OpaqueRepository<T> {
    override fun get(): T? {
        val coll = getCollection()
        return coll.find(BsonDocument(), clz).first()
    }

    override fun save(what: T) {
        val coll = getCollection()
        coll.replaceOne(BsonDocument(), what!!, ReplaceOptions.createReplaceOptions(UpdateOptions().upsert(true)))
    }

    private fun getCollection(): MongoCollection<T> {
        val client = KMongo.createClient(ConnectionString(config.connectionString))
        val db = client.getDatabase(config.databaseName)
        val coll = db.getCollection(collectionName, clz)
        return coll!!
    }
}
