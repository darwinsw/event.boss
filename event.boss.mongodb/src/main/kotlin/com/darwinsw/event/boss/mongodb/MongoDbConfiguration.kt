package com.darwinsw.event.boss.mongodb

data class MongoDbConfiguration(val connectionString : String, val databaseName: String)