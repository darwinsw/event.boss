package com.darwinsw.event.boss.core

import com.darwinsw.event.boss.core.account.ObjectMother.avatar
import org.junit.Test
import org.junit.Assert.*
import org.hamcrest.Matchers.*
import com.darwinsw.event.boss.core.account.ObjectMother.OneId
import com.darwinsw.event.boss.core.account.ObjectMother.Martinellip
import com.darwinsw.event.boss.core.account.ObjectMother.now
import com.darwinsw.event.boss.core.account.ObjectMother.Pietrom
import com.darwinsw.event.boss.core.account.ObjectMother.DonaldDuck
import com.darwinsw.event.boss.core.account.*

class UserAccountTest {
    @Test
    fun create() {
        val account = UserAccount(OneId, now, Pietrom, avatar)
        assertThat(account.uncommittedChanged().size, `is`(equalTo(1)))
        assertThat(account.avatar, `is`(equalTo(avatar)))
    }

    @Test
    fun changeUsername() {
        val account = UserAccount(OneId, now, Pietrom, avatar)
        account.changeUsername(Martinellip)
        assertThat(account.username, `is`(equalTo(Martinellip)))
    }

    @Test
    fun chooseAvatar() {
        val account = UserAccount(OneId, now, Pietrom, avatar)
        account.chooseAvatar(DonaldDuck)
        assertThat(account.avatar, `is`(equalTo(DonaldDuck)))
    }

    @Test
    fun replayShouldSetId() {
        val events = listOf(
                UserAccountCreated(OneId, now, Pietrom, avatar),
                UsernameChanged(OneId, Martinellip),
                AvatarChosen(OneId, DonaldDuck),
                AvatarChosen(OneId, avatar),
                AvatarChosen(OneId, DonaldDuck),
                UsernameChanged(OneId, Pietrom)
        )
        val account = UserAccount(events)
        assertThat(account.id, `is`(equalTo(OneId)))
    }

    @Test
    fun replayShouldSetVersion() {
        val events = listOf(
                UserAccountCreated(OneId, now, Pietrom, avatar),
                UsernameChanged(OneId, Martinellip),
                AvatarChosen(OneId, DonaldDuck),
                AvatarChosen(OneId, avatar),
                AvatarChosen(OneId, DonaldDuck),
                UsernameChanged(OneId, Pietrom)
        )
        val account = UserAccount(events)
        assertThat(account.version, `is`(equalTo(events.size)))
    }

    @Test
    fun replayShouldLeaveNoUncommittedChanges() {
        val events = listOf(
                UserAccountCreated(OneId, now, Pietrom, avatar),
                UsernameChanged(OneId, Martinellip),
                AvatarChosen(OneId, DonaldDuck),
                AvatarChosen(OneId, avatar),
                AvatarChosen(OneId, DonaldDuck),
                UsernameChanged(OneId, Pietrom)
        )
        val account = UserAccount(events)
        assertThat(account.uncommittedChanged(), `is`(empty()))
    }

    @Test
    fun replayEvents() {
        val events = listOf(
                UserAccountCreated(OneId, now, Pietrom, avatar),
                UsernameChanged(OneId, Martinellip),
                AvatarChosen(OneId, DonaldDuck),
                AvatarChosen(OneId, avatar),
                AvatarChosen(OneId, DonaldDuck),
                UsernameChanged(OneId, Pietrom)
        )
        val account = UserAccount(events)
        assertThat(account.username, `is`(equalTo(Pietrom)))
        assertThat(account.avatar, `is`(equalTo(DonaldDuck)))
    }
}