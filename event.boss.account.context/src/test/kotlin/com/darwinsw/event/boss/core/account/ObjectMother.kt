package com.darwinsw.event.boss.core.account

import java.time.ZonedDateTime
import java.util.*

object ObjectMother {
    val uuid = UUID.randomUUID()
    val OneId = UserId(uuid)
    val Pietrom = Username("pietrom")
    val Martinellip = Username("martinellip")
    val now = ZonedDateTime.now()
    val avatar = "my-avatar.png"
    val DonaldDuck = "my-new-avatar.png"
}