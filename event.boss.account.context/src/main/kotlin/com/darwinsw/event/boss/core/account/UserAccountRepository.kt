package com.darwinsw.event.boss.account.context

import com.darwinsw.event.boss.core.account.UserAccount
import com.darwinsw.event.boss.core.account.UserId

interface UserAccountRepository {
    fun findById(id : UserId) : UserAccount?

    fun save(account: UserAccount)
}
