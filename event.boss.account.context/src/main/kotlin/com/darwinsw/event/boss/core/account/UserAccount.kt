package com.darwinsw.event.boss.core.account

import com.darwinsw.event.boss.core.es.*
import java.time.ZonedDateTime
import java.util.*

class UserAccount(val id: UserId, val createdOn: ZonedDateTime, username : Username, avatar: String?) : EventSourcedResource<UserId>() {
    init {
        publish(UserAccountCreated(id, createdOn, username, avatar))
    }
    constructor(id: UserId, createdOn: ZonedDateTime, username : Username) : this(id, createdOn, username, null)

    constructor(events: List<DomainEvent<UserId>>) : this(firstEventAs<UserAccountCreated>(events).id,
            firstEventAs<UserAccountCreated>(events).createdAt,
            firstEventAs<UserAccountCreated>(events).username,
            firstEventAs<UserAccountCreated>(events).avatar) {
        replayTail(events)
    }

    var username: Username = username
        private set

    var avatar : String? = avatar
        private set

    val preferences : MutableList<UserPreference> = mutableListOf()

    fun chooseAvatar(newAvatar : String) {
        applyAndPublish(AvatarChosen(id, newAvatar))
    }

    fun changeUsername(newUsername: Username) {
        applyAndPublish(UsernameChanged(id, newUsername))
    }

    fun addPreference(pref : UserPreference) {
        applyAndPublish(UserPreferenceAdded(id, pref))
    }

    private fun on(evt: UsernameChanged) {
        username = evt.username
    }

    private fun on(evt: AvatarChosen) {
        avatar = evt.avatar
    }

    private fun on(evt: UserPreferenceAdded) {
        preferences.add(evt.preference)
    }
}
data class UserId(override val value: UUID) : ResourceId
data class Username(val value: String)
data class UserPreference(val key: String, val value: String)

data class UserAccountCreated(override val id: UserId, val createdAt: ZonedDateTime, val username: Username, val avatar : String?) : DomainEvent<UserId> {
    constructor(id: UserId, createdAt: ZonedDateTime, username: Username) : this(id, createdAt, username, null)
}
data class AvatarChosen(override val id: UserId, val avatar: String) : DomainEvent<UserId>
data class UsernameChanged(override val id: UserId, val username: Username) : DomainEvent<UserId>
data class UserPreferenceAdded(override val id: UserId, val preference: UserPreference): DomainEvent<UserId>