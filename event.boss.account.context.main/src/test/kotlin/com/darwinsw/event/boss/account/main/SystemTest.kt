package com.darwinsw.event.boss.account.main

import com.darwinsw.event.boss.account.persistence.es.EventStoreBasedUserAccountRepository
import com.darwinsw.event.boss.core.account.*
import com.darwinsw.event.boss.core.es.DomainEvent
import com.darwinsw.event.boss.esjc.EventStoreBasedEventDispatcher
import com.darwinsw.event.boss.esjc.EventStoreBasedEventStore
import com.darwinsw.event.boss.gson.GsonBasedDomainEventSerializer
import com.darwinsw.event.boss.rabbitmq.RabbitMqEventSubscriber
import com.darwinsw.event.boss.rabbitmq.RabbitMqMessagePublisher
import org.awaitility.Awaitility.await
import org.junit.*
import java.time.*
import java.util.*
import java.util.concurrent.TimeUnit

class SystemTest {
    private val serializer = GsonBasedDomainEventSerializer()
    private val cfg = EventStoreConnectionHelper.readEventStoreConfiguration()
    private val store = EventStoreBasedEventStore(cfg, serializer)
    private val repo = EventStoreBasedUserAccountRepository(store)
    private val publisher = RabbitMqMessagePublisher(RabbitMqConnectionHelper.readRabbitMqConfiguration())
    private val subscriber0 = RabbitMqEventSubscriber(RabbitMqConnectionHelper.readRabbitMqConfiguration(), serializer)
    private val subscriber1 = RabbitMqEventSubscriber(RabbitMqConnectionHelper.readRabbitMqConfiguration(), serializer, "App01")
    private val dispatcher = EventStoreBasedEventDispatcher(cfg, serializer, publisher, PositionRepository(MongoDbConnectionHelper.readMongoDbConfiguration()))
    private val avatarChangesCountPerAccount = 3
    private val accountCount = 3
    private val timeout = 15L

    @Before
    fun init() {
        publisher.connect()
        subscriber0.connect()
        dispatcher.start()
    }

    @Test
    fun publishMessagesWhenAggregateIsSaved() {
        var avatarEvtCount = 0
        subscriber0.subscribe(AvatarChosen::class.java) { evt ->
            avatarEvtCount++
        }
        var createdEvtCount = 0
        subscriber0.subscribe(UserAccountCreated::class.java) { evt ->
            createdEvtCount++
        }
        var allEvtCount0 = 0
        subscriber0.subscribe(DomainEvent::class.java) { evt ->
            allEvtCount0++
        }
        var allEvtCount1 = 0
        subscriber1.subscribe(DomainEvent::class.java) { evt ->
            allEvtCount1++
        }
        var allEvtCount1named = 0
        subscriber1.subscribe("Second", DomainEvent::class.java) { evt ->
            allEvtCount1named++
        }

        var allEvtCompetingCount0 = 0
        var allEvtCompetingCount1 = 0
        subscriber0.subscribe("Competing", DomainEvent::class.java) { evt ->
            allEvtCompetingCount0++
        }
        subscriber0.subscribe("Competing", DomainEvent::class.java) { evt ->
            allEvtCompetingCount1++
        }

        for(i in 1..accountCount) {
            createAndChangeAccount("username${i}")
        }

        val expectedAvatarChosenCount = avatarChangesCountPerAccount * accountCount
        val expectedAccountCreatedCount = accountCount
        val expectedEventCount =  expectedAccountCreatedCount + expectedAvatarChosenCount

        await().atMost(timeout, TimeUnit.SECONDS).until { expectedAvatarChosenCount == avatarEvtCount }

        await().atMost(timeout, TimeUnit.SECONDS).until { expectedAccountCreatedCount == createdEvtCount }

        await().atMost(timeout, TimeUnit.SECONDS).until { expectedEventCount == allEvtCount0 }

        await().atMost(timeout, TimeUnit.SECONDS).until { expectedEventCount == allEvtCount1 }

        await().atMost(timeout, TimeUnit.SECONDS).until { expectedEventCount == (allEvtCompetingCount0 + allEvtCompetingCount1) }

        await().atMost(timeout, TimeUnit.SECONDS).until { expectedEventCount == allEvtCount1named }
    }

    private fun createAndChangeAccount(username: String) {
        val account = UserAccount(UserId(UUID.randomUUID()), ZonedDateTime.now(), Username(username))

        for (i in 1..avatarChangesCountPerAccount) {
            account.chooseAvatar("${username}_img_${i}.png")
        }
        repo.save(account)
    }

    @After
    fun cleanup() {
        store.stop()
        dispatcher.stop(Duration.ofSeconds(10))
        subscriber0.disconnect()
        publisher.disconnect()
    }
}