package com.darwinsw.event.boss.account.main

import com.darwinsw.event.boss.account.main.MongoDbConnectionHelper.readMongoDbConfiguration
import com.darwinsw.event.boss.esjc.EventStorePosition
import com.darwinsw.event.boss.esjc.EventStorePositionRepository
import com.darwinsw.event.boss.mongodb.MongoDbConfiguration
import com.darwinsw.event.boss.mongodb.MongoDbOpaqueRepository
import com.github.msemys.esjc.Position
import com.mongodb.client.MongoClient
import org.bson.codecs.configuration.CodecRegistry
import org.bson.codecs.pojo.PojoCodecProvider
import org.bson.codecs.configuration.CodecProvider
import org.bson.codecs.configuration.CodecRegistries.fromProviders
import org.bson.codecs.configuration.CodecRegistries.fromRegistries


object MongoDbConnectionHelper  {
    fun readMongoDbConfiguration() : MongoDbConfiguration {
        return MongoDbConfiguration(readConnectionString(), "event_boss_infrastructure")
    }

    private fun readConnectionString() = System.getenv("MONGODB_CONNECTION_STRING") ?: "mongodb://localhost/event_boss"
}

class PositionRepository(config : MongoDbConfiguration) : MongoDbOpaqueRepository<EventStorePosition>(config, "_EventStorePosition", EventStorePosition::class.java), EventStorePositionRepository