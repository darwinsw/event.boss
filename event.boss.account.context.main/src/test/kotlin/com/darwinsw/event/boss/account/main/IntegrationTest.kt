package com.darwinsw.event.boss.account.main

import com.darwinsw.event.boss.account.persistence.es.EventStoreBasedUserAccountRepository
import com.darwinsw.event.boss.core.account.*
import com.darwinsw.event.boss.gson.GsonBasedDomainEventSerializer
import com.darwinsw.event.boss.esjc.EventStoreBasedEventStore
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.equalTo
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Test
import java.time.ZonedDateTime
import java.util.*

class IntegrationTest {
    private val userId = UserId(UUID.randomUUID())
    private val serializer = GsonBasedDomainEventSerializer()
    private val cfg = EventStoreConnectionHelper.readEventStoreConfiguration()
    private val store = EventStoreBasedEventStore(cfg, serializer)

    @Test
    fun createSaveReloadAggregate() {
        val account = UserAccount(userId, ZonedDateTime.now(), Username("pietrom"))
        account.changeUsername(Username("martinellip"))
        account.chooseAvatar("myimage.png")
        account.changeUsername(Username("pietrom"))
        val changesCount = 200
        for (i in 1..changesCount) {
            account.chooseAvatar("myimage_${i}.png")
            account.addPreference(UserPreference("pref${i}", i.toString()))
        }

        val repo = EventStoreBasedUserAccountRepository(store)
        repo.save(account)

        val reloaded = repo.findById(userId) !!

        assertThat(reloaded.version, `is`(equalTo(account.version)))
        assertThat(reloaded.username, `is`(equalTo(account.username)))
        assertThat(reloaded.avatar, `is`(equalTo(account.avatar)))
        assertThat(reloaded.preferences, `is`(equalTo(account.preferences)))
    }

    @After
    fun cleanup() {
        store.truncate(EventStoreBasedUserAccountRepository.streamIdOf(userId))
        store.stop()
    }
}