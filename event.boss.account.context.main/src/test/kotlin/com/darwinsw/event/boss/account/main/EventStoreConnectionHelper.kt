package com.darwinsw.event.boss.account.main

import com.darwinsw.event.boss.esjc.EventStoreConfiguration

object EventStoreConnectionHelper {
    fun readEventStoreConfiguration() : EventStoreConfiguration {
        val host = System.getenv("EVENT_STORE_HOST") ?: "localhost"
        val port = System.getenv("EVENT_STORE_PORT") ?: "1113"
        val username = System.getenv("EVENT_STORE_USERNAME") ?: "admin"
        val password = System.getenv("EVENT_STORE_PASSWORD") ?: "changeit"
        return EventStoreConfiguration(host, Integer.parseInt(port), username, password)
    }
}