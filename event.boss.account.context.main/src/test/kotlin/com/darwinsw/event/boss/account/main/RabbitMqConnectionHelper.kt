package com.darwinsw.event.boss.account.main

import com.darwinsw.event.boss.rabbitmq.RabbitMqConfiguration

object RabbitMqConnectionHelper {
    fun readRabbitMqConfiguration() : RabbitMqConfiguration = RabbitMqConfiguration(readConnectionString())

    private fun readConnectionString() = System.getenv("RABBITMQ_CONNECTION_STRING") ?: "amqp://guest:guest@localhost"
}