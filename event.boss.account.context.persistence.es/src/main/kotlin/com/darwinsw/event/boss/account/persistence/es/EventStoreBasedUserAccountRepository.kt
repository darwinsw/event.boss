package com.darwinsw.event.boss.account.persistence.es

import com.darwinsw.event.boss.account.context.UserAccountRepository
import com.darwinsw.event.boss.core.account.UserAccount
import com.darwinsw.event.boss.core.account.UserId
import com.darwinsw.event.boss.core.es.DomainEvent
import com.darwinsw.event.boss.core.es.EventStore
import com.darwinsw.event.boss.core.es.StreamId

class EventStoreBasedUserAccountRepository(private val store : EventStore) : UserAccountRepository {
    override fun save(account: UserAccount) {
        store.save(streamIdOf(account.id), account.uncommittedChanged())
    }

    override fun findById(id: UserId) : UserAccount? {
        val events = store.load(streamIdOf(id))
        return UserAccount(events as List<DomainEvent<UserId>>)
    }

    companion object {
        fun streamIdOf(id: UserId) = StreamId("UserAccount", id)
    }
}
