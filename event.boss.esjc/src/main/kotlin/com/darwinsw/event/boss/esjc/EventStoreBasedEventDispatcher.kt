package com.darwinsw.event.boss.esjc

import com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es.DomainEventSerializer
import com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es.MessagePublisher
import com.github.msemys.esjc.CatchUpSubscription
import com.github.msemys.esjc.CatchUpSubscriptionSettings
import com.github.msemys.esjc.Position
import java.time.Duration

class EventStoreBasedEventDispatcher(cfg: EventStoreConfiguration, private val serializer : DomainEventSerializer, private val publisher : MessagePublisher, private val repo : EventStorePositionRepository) {
    private val eventStore = EventStoreHelper.createEventStore(cfg)
    private var subscription : CatchUpSubscription? = null
    private var lastPos : Position = Position.START

    fun start() {
        val initialPos = repo.get()?.toPosition() ?: Position.START
        subscription = eventStore.subscribeToAllFrom(initialPos, CatchUpSubscriptionSettings.newBuilder()
            .resolveLinkTos(true).build()) { sub, evt ->
                if(!evt.event.eventType.startsWith("$") && evt.link == null) {
                    publisher.publish(evt.event.eventType, evt.event.data)
                }
                lastPos = evt.originalPosition
                repo.save(EventStorePosition.fromPosition(lastPos))
            }
    }

    fun stop(timeout : Duration = Duration.ofSeconds(3)) {
        subscription?.stop(timeout)
        subscription?.close()
        eventStore.shutdown()
    }
}