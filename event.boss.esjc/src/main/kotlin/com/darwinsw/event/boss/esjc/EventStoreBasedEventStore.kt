package com.darwinsw.event.boss.esjc

import com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es.DomainEventSerializer
import com.darwinsw.event.boss.core.es.DomainEvent
import com.darwinsw.event.boss.core.es.EventStore
import com.darwinsw.event.boss.core.es.StreamId
import com.darwinsw.event.boss.esjc.EventStoreHelper.createEventStore
import com.github.msemys.esjc.EventData
import com.github.msemys.esjc.ExpectedVersion
import com.github.msemys.esjc.StreamEventsSlice
import java.util.concurrent.CompletableFuture

class EventStoreBasedEventStore(cfg: EventStoreConfiguration, private val serializer : DomainEventSerializer) : EventStore {
    companion object {
        const val ChunkSize : Int = 100
    }

    private val eventStore = createEventStore(cfg)

    private fun loadSliceAsync(id: StreamId, from: Long, to:Long, sliceSize: Int, oldEvents: List<DomainEvent<*>>) : CompletableFuture<List<DomainEvent<*>>> {
        val slice = eventStore.readStreamEventsForward(id.streamId, from, sliceSize, false)
        return slice.thenCompose { res ->
            val events = oldEvents + slice2list(res)
            if(res.isEndOfStream || res.nextEventNumber > to)
                CompletableFuture.completedFuture(events)
            else loadSliceAsync(id, from + sliceSize, to, sliceSize, events)
        }
    }

    override fun load(id: StreamId): List<DomainEvent<*>> {
        return loadSliceAsync(id, 0, Long.MAX_VALUE, ChunkSize, listOf()).get()
    }

    private fun slice2list(res: StreamEventsSlice): List<DomainEvent<*>> {
        return res.events.map { evt ->
            val type = evt.originalEvent().eventType
            val data = String(evt.originalEvent().data)
            rehydrateDomainEvent(data, type)
        }
    }

    private fun rehydrateDomainEvent(data: String, type: String?) =
            serializer.deserialize(data, Class.forName(type)) as DomainEvent<*>

    override fun save(id: StreamId, events: List<DomainEvent<*>>) {
        var jsonData = events
                .map { evt -> EventData.newBuilder()
                        .type(evt.javaClass.kotlin.qualifiedName)
                        .jsonData(serializer.serialize(evt)).build() }

        eventStore.appendToStream(id.streamId, ExpectedVersion.ANY, jsonData).join()
    }

    fun truncate(id: StreamId) {
        eventStore.deleteStream(id.streamId, ExpectedVersion.ANY).join()
    }

    fun stop() {
        eventStore.shutdown()
    }
}
