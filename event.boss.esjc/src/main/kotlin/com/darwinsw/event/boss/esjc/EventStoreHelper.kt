package com.darwinsw.event.boss.esjc

import com.github.msemys.esjc.EventStoreBuilder

object EventStoreHelper {
    fun createEventStore(config: EventStoreConfiguration) : com.github.msemys.esjc.EventStore {
        return EventStoreBuilder.newBuilder()
                .singleNodeAddress(config.host, config.port)
                .userCredentials(config.username, config.password)
                .build()
    }
}