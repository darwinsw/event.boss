package com.darwinsw.event.boss.esjc

data class EventStoreConfiguration(val host: String, val port: Int, val username: String, val password: String)