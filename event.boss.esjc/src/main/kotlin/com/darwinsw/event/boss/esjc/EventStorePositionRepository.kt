package com.darwinsw.event.boss.esjc

import com.darwinsw.event.boss.core.persistence.OpaqueRepository
import com.github.msemys.esjc.Position

interface EventStorePositionRepository: OpaqueRepository<EventStorePosition>

data class EventStorePosition(val commitPosition: Long, val preparePosition: Long) {
    val _id : String = "LastPosition"
    fun toPosition() : Position = Position(commitPosition, preparePosition)

    companion object {
        fun fromPosition(pos: Position) : EventStorePosition = EventStorePosition(pos.commitPosition, pos.preparePosition)
    }
}