package com.darwinsw.event.boss.gson

import com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es.DomainEventSerializer
import com.darwinsw.event.boss.core.es.DomainEvent
import com.google.gson.GsonBuilder
import java.time.ZonedDateTime

class GsonBasedDomainEventSerializer : DomainEventSerializer {
    override fun <TEvent> deserialize(data: String, clazz: Class<TEvent>?): TEvent {
        return gson.fromJson(data, clazz)
    }

    private val gson =  GsonBuilder()
            .registerTypeAdapter(ZonedDateTime::class.java, ZonedDateTimeSerializer())
            .create()

    override fun serialize(evt: DomainEvent<*>): String {
        return gson.toJson(evt)
    }
}