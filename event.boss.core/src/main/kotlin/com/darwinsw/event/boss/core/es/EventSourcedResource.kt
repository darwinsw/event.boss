package com.darwinsw.event.boss.core.es

import java.lang.reflect.InvocationTargetException
import kotlin.reflect.full.declaredFunctions
import kotlin.reflect.jvm.isAccessible

abstract class EventSourcedResource<TKey> {
    private val changes: MutableList<DomainEvent<TKey>> = mutableListOf()
    var version : Int = 1
        private set

    fun publish(evt: DomainEvent<TKey>) {
        changes.add(evt)
    }

    fun applyAndPublish(evt: DomainEvent<TKey>) {
        apply(evt)
        publish(evt)
    }

    fun applyAll(events: List<DomainEvent<TKey>>) {
        for (evt in events) {
            apply(evt)
        }
    }

    fun apply(evt: DomainEvent<TKey>) {
        try {
            val method = this.javaClass.kotlin
                .declaredFunctions.filter {
                    f -> f.name == "on" && f.parameters.size == 2 && f.parameters[1].type.classifier == evt.javaClass.kotlin
                }.singleOrNull()

            method?.isAccessible = true
            method?.call(this, evt)
            version++
        } catch (e: IllegalAccessException) {
            e.printStackTrace()
        } catch (e: InvocationTargetException) {
            e.printStackTrace()
        } catch (e: NoSuchMethodException) {
            e.printStackTrace()
        }
    }

    fun uncommittedChanged() = changes

    fun replayTail(events : List<DomainEvent<TKey>>) {
        applyAll(events.drop(1))
        commit()
    }

    fun commit() {
        uncommittedChanged().clear()
    }

    companion object {
        fun <TEvent> firstEventAs(events: List<DomainEvent<*>>) : TEvent where TEvent : DomainEvent<*> {
            return events.first() as TEvent
        }
    }
}