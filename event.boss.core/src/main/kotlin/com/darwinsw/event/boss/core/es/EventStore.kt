package com.darwinsw.event.boss.core.es

interface EventStore {
    fun load(id: StreamId) : List<DomainEvent<*>>

    fun save(id: StreamId, events: List<DomainEvent<*>>)
}

