package com.darwinsw.event.boss.core.persistence

interface OpaqueRepository<T : Any> {
    fun save(what: T)

    fun get() : T?
}