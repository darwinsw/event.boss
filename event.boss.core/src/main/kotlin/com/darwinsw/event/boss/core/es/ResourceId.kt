package com.darwinsw.event.boss.core.es

import java.util.*

interface ResourceId {
    val value : UUID
}