package com.darwinsw.event.boss.core.es

data class StreamId(val streamType: String, val aggregateId: ResourceId) {
    val streamId = "${streamType}.${aggregateId.value}"
}