package com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.persistence

interface Repository<TKey, TItem> {
    fun save(what: TItem) : TKey

    fun get(key: TKey) : TItem
}