package com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es

import com.darwinsw.event.boss.core.es.DomainEvent

interface MessagePublisher {
    //fun <TEvent : DomainEvent<*>> publish(evt: TEvent)
    fun publish(msgType: String, data: ByteArray)
}