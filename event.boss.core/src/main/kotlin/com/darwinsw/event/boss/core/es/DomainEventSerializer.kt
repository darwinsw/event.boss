package com.darwinsw.event.boss.core.com.darwinsw.event.boss.core.es

import com.darwinsw.event.boss.core.es.DomainEvent

interface DomainEventSerializer {
    fun serialize(evt: DomainEvent<*>) : String

    fun <TEvent> deserialize(data: String, forName: Class<TEvent>?): TEvent
}