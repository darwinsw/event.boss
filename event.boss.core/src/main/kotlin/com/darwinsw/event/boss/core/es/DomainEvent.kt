package com.darwinsw.event.boss.core.es

interface DomainEvent<TKey> {
    val id : TKey
}