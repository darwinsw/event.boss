package com.darwinsw.event.boss.core
import org.junit.Test
import org.junit.Assert.*
import org.hamcrest.Matchers.*

class UniverseTest {
    @Test
    fun theUltimateQuestionOfLifeTheUniverseAndEverything() {
        assertThat(Universe.answer(), `is`(equalTo(42)))
    }
}